// Send search input from the genome dashboard to the chrome extension
console.log('IGB web app extension content script successfully injected');
const searchInput = document.querySelector('.searchbar input');
const extensionId = 'ffogppflbcnhkfegojemcfkgacopflia';

// Search input is sent every time the input text changes
searchInput.onkeyup = (event) => {
  chrome.runtime.sendMessage(extensionId, { userSearch: searchInput.value }, response => {
    console.log(response);
  });
}
