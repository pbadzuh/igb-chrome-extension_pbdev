const checkIgbStatusButton = document.getElementById('checkIgbStatus');
const igbStatusParagraph = document.getElementById('igbStatus');

checkIgbStatusButton.onclick = () => {
  fetch('http://127.0.0.1:7085/igbStatusCheck')
    .then(res => {
      if (res.status == 200) {
        igbStatusParagraph.style.color = 'green';
        igbStatusParagraph.textContent = 'IGB is running'
      }
    })
    .catch(err => {
      igbStatusParagraph.style.color = 'red';
      igbStatusParagraph.textContent = 'IGB is not running'
    });
}
